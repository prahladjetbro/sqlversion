<!DOCTYPE html>
<html>
    <?php 
        include_once dirname(__FILE__)."/config.php"; 
        $connection = mysqli_connect($databasehost, $databaseusername, $databasepassword, $database); // or die('Not Connected');
        
        define('MODE_RUN', $_GET['run'] === 'true');
        define('MODE_PROD', $database === 'sqlversion'); 
        if(!file_exists($serverpath."log/sqlfile_log.csv")){ 
            file_put_contents($serverpath."log/sqlfile_log.csv",""); 
        }
        $file_handle = fopen($serverpath."log/sqlfile_log.csv","r");
        $fileLogData =[];
        if(isset($file_handle))
        {
            $logdata = array();
            $fields=array('file','lastrun');
            $i=0;
            while(!feof($file_handle)) 
            {
                $line_of_text = fgetcsv($file_handle,2048);   
                if(sizeof($line_of_text)>0)
                {
                    foreach ($fields as $fkey => $fval) {
                        $logdata[$i][$fval]=$line_of_text[$fkey]; 
                    } 
                }
                $i++;
            } 
            foreach($logdata as $k => $v){
                if(trim($v['lastrun']) != "")
                {
                    $fileLogData[$v['file']]=$v['lastrun'];
                }
            } 
        } 
         
        $updates = [];
        $updateFolders = scandir('./database/updates');

        foreach ($updateFolders as $updateFolder) {
            if ($updateFolder[0] === '.') continue;

            $updates[$updateFolder] = [];

            $updateScripts = scandir("./database/updates/{$updateFolder}");
             // var_dump($updateScripts);
            foreach ($updateScripts as $updateScript) { 
                // echo strtolower($updateScript, -4);
                if ($updateScript[0] === '.' || substr(strtolower($updateScript), -4) !== '.sql')continue ; 
                if(isset($fileLogData["./database/updates/{$updateFolder}/".$updateScript]))
                {
                        $fileUpdateTime =  date ("Y-m-d H:i:s.", filemtime("./database/updates/{$updateFolder}/".$updateScript)); 
                        $fileRunTime =  date ("Y-m-d H:i:s", strtotime($fileLogData["./database/updates/{$updateFolder}/".$updateScript]));
                        $updates[$updateFolder][] = array('path'=>$updateScript,'isrun'=>(strtotime($fileUpdateTime) > strtotime($fileRunTime) ? 'N': 'Y'),'runtime'=>$fileRunTime); 
                } 
                else{ 
                    $updates[$updateFolder][] = array('path'=>$updateScript,'isrun'=> 'N','runtime'=>'');
                }
            }
        } 
    ?>

    <head>
        <title>DataBase updater v0.1 </title>
        <style>
            body {
                padding: 40px;
                background-color: #111;
                color: #aaa;
                font-family: monospace;
            }
            hr{
                margin: 0 auto;
                max-width: 70%;
            }
            table {
                border-collapse: collapse;
                margin: 0 auto;
                min-width: 400px;
                max-width: 90%;
            }
            table table {
                min-width: auto;
            }

            th {
                background-color: #ccc;
                color: #000;
            }

            td, th {
                text-align: left;
                padding: 0.5em 2em;
                font-size: 14px;
                color: #000;
                background: #fff;
            }

            a {
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            .r { color: #f66; }
            .g { color: #6f6; }
            .y { color: #ff6; }
            .s { color: #666; }

            .rh { background-color: #f66; color: #000; font-weight: bold; }
            .gh { background-color: #6f6; color: #000; font-weight: bold; }
            .yh { background-color: #ff6; color: #000; font-weight: bold; }
            .sh { background-color: #666; color: #000; font-weight: bold; }

            #run {
                display: inline-block;
                height: auto;
                width: auto;
                text-align: center;
                vertical-align: middle;
                line-height: 2em;
                text-decoration: none;
                margin: 0 auto;
                padding: 0.4em;
            }

            #run:hover {
                background-color: #afa;
            }

            .script-row {
                border-top: solid 1px #aaa;
            }

            .script-row:first {
                border-top: none;
            } 
         </style>
    </head>

    <body>
        <table>
                <tr>
                    <th colspan="2">Database</th> 
                </tr>
                <tr>
                    <td>Host</td>
                    <td><?=$databasehost ?></td>
                </tr>
                <tr>
                    <td>Database</td>
                    <td class="<?=(MODE_PROD ? 'gh' : 'rh') ?>"><?= $database ?></td>
                </tr>
                <?php if(!MODE_PROD){?>
                    <tr>
                        <td colspan="2">Please make sure database connection correct !!!</td> 
                    </tr>
                <?php } ?>
        </table> 
        <br></br>
        <hr >
        <br>
        
        <table>
        <?php

        $successes = 0;
        $errors = 0;
        echo "<tr><th>File Name</th><th colspan=\"2\"> Last Run Time</th></tr>"; 
        foreach ($updates as $update => $scripts) { 
            echo "<tr><th colspan=\"3\">Batch {$update}</th></tr>"; 
            foreach ($scripts as $script) {
                echo '<tr class="script-row">'; 
                $resultRenders = [];
                if (MODE_RUN) {
                    if($script['isrun'] == 'N')
                    {
                        $results = runScript("./database/updates/{$update}/{$script['path']}");
                        foreach ($results as $result) {
                            if (is_int($result)) {
                                $resultRenders[] = new ResultRender(ResultRender::STATUS_SUCCESS, $result);
                            } else {
                                $resultRenders[] = new ResultRender(ResultRender::STATUS_ERROR, $result);
                            }
                        } 
                    }
                } else { 
                    //$resultRenders[] = new ResultRender(($script['isrun'] == 'Y') ? ResultRender::STATUS_RUN : ResultRender::STATUS_NOT_RUN, null);
                } 
                echo "<td>{$script['path']}</td>";  
                $tempLastRun =($script['isrun'] == 'Y') ? "Run At : ". date('d M Y H:i',strtotime($script['runtime'])) : "Not run yet";
                echo "<td> {$tempLastRun} </td>";
                echo '<td><table style="padding: 5px 0;">';
                foreach ($resultRenders as $resultRender) echo $resultRender;
                
                echo '</table></td>';

                echo '</tr>';
            }
        }

        ?>
        </table>
        <br></br>
        <hr >
        <br>
        <table>
            <?php if(!MODE_RUN) { ?>
                <td rowspan="3"> 
                    <a href="?run=true" class="gh" id="run">RUN ALL SQL</a>
                </td>
                <td rowspan="3">
                    Click <span class="gh"> RUN ALL SQL</span>  to run all database update scripts.
                    <br>Each numbered folder in /db/updates/ will be
                    <br>opened in order and the scripts there in run.

                </td>
           <?php }  ?>
        </table>
        <table>  
           <?php 
            if (MODE_RUN) {
                updateLog();
            }
            if (MODE_RUN) {
        ?>
            <tr>
                <th colspan="3">Result Summary</th>
            </tr>
            <tr>
                <td>Success</td>
                <td class="g"><?= ResultRender::$successes ?></td>
            </tr>
            <tr>
                <td>Error</td>
                <td class="r"><?= ResultRender::$errors ?></td>
            </tr>
            <tr>
                <td>Total</td>
                <td class=""><?= ResultRender::$successes + ResultRender::$errors ?></td>
            </tr>
        <?php } ?>
        </table>
        <br> 
    </body>
    <script>
        history.replaceState(null, null, location.href.split('?')[0]);
    </script>
</html>
<?php

function runScript($path) {
    global $connection,$fileLogData; 
    $sql = file_get_contents($path);  
    $fileLogData[$path]=date('Y-m-d H:i:s');
    fwrite($file_handle, $path.",".date('Y-m-d H:i:s')); 
    $queries = explode(';', $sql);
    $results = [];

    foreach ($queries as $query){

        if (!trim($query)) continue;

        mysqli_query($connection, $query . ';');
    
        $result = mysqli_error($connection);
        if (!$result) $result = mysqli_affected_rows ($connection);

        $results[] = $result;
    }

    return $results;
}
function updateLog(){ 
    global $serverpath,$fileLogData; 
    $file_handle = fopen($serverpath."log/sqlfile_log.csv","a");  
    $resultsdata = array();
    foreach ($fileLogData as $key => $v) 
    { 
        $resultsdata[] = array($key,$v);
    }  
    foreach ($resultsdata as $k => $v) 
    { 
        fputcsv($file_handle, $v,',', '"'); 
    }
}

class ResultRender {
    
    const STATUS_SUCCESS    = 'SUCCESS';
    const STATUS_ERROR      = '!ERROR!';
    const STATUS_NOT_RUN    = 'NOT RUN';
    const STATUS_RUN        = 'RUN';

    static public $errors;
    static public $successes;

    private $status;
    private $result;
    private $comment;

    function __construct($status, $result) {
        $this->status = $status;
        $this->result = $result;
        $this->comment = is_numeric($result) ? $result . ' rows affected.' : $result;

        switch($status) {
            case self::STATUS_ERROR: self::$errors++; break;
            case self::STATUS_SUCCESS: self::$successes++; break;
        }
    }

    function __toString() {
        switch ($this->status) {
            case self::STATUS_ERROR :
                $badgeClass = 'rh';
                $commentClass = 'r';
                break;
            case self::STATUS_SUCCESS :
                $badgeClass = 'gh';
                $commentClass = $this->result ? 'g' : '';
                break;
            case self::STATUS_NOT_RUN :
                $badgeClass = 'sh';
                $commentClass = '';
                break;
                case self::STATUS_RUN :
                    $badgeClass = 'gh';
                    $commentClass = $this->result ? 'g' : '';
                    break;
        }

        return "<tr><td class=\"{$badgeClass}\">{$this->status}</td>"
            . "<td class=\"{$commentClass}\">{$this->comment}</td></tr>";
    }
}
