FROM php:7.4.8-apache
LABEL Name=nlusearch Version=0.0.1 
RUN cp /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" 

RUN apt-get -y update && pecl install xdebug \
    && docker-php-ext-enable xdebug
COPY docker/config/xdebug.ini $PHP_INI_DIR/conf.d/xdebug.ini

CMD ["apache2ctl", "-k", "restart", "-D", "FOREGROUND"]